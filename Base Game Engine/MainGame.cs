﻿using Base_Game_Engine.Scenes;
using GeonBit.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Base_Game_Engine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : Game
    {
        public static MainGame instance;

        protected GraphicsDeviceManager graphics;
        protected SpriteBatch spriteBatch;

        protected Scene currentScene;

        public MainGame()
        {
            instance = this;

            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // Initialisation de l'interface
            UserInterface.Initialize(Content, BuiltinThemes.hd);
            
            // Chargement de la scène du menu
            ChangeScene(new MenuScene());

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            //    Exit();

            // TODO: Add your update logic here
            UserInterface.Active.Update(gameTime);

            if (currentScene != null)
            {
                currentScene.UpdateEngine((float)gameTime.ElapsedGameTime.TotalSeconds);
                currentScene.Update((float) gameTime.ElapsedGameTime.TotalSeconds);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            if (currentScene != null)
            {
                currentScene.DrawEngine(spriteBatch);
                currentScene.Draw(spriteBatch);
            }

            spriteBatch.End();

            UserInterface.Active.Draw(spriteBatch);

            base.Draw(gameTime);
        }

        public void ChangeScene(Scene pScene)
        {
            if (currentScene != null)
            {
                currentScene.Unload();
            }

            currentScene = pScene;
            currentScene.InitEngine();
            currentScene.Load();
        }

        public Scene GetCurrentScene()
        {
            return currentScene;
        }
    }
}
