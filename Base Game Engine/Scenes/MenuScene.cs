﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using System;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Xml;

namespace Base_Game_Engine.Scenes
{
    public class MenuScene : Scene
    {
        protected Button keyUpButton;
        protected Button keyDownButton;
        protected Button keyLeftButton;
        protected Button keyRightButton;

        public override void Load()
        {
            // Menu
            Panel mainPanel = new Panel(new Vector2(400, 300), PanelSkin.Default, Anchor.Center);
            UserInterface.Active.AddEntity(mainPanel);

            Header header = new Header("Untitled Game");
            mainPanel.AddChild(header);

            HorizontalLine horizontalLine = new HorizontalLine();
            mainPanel.AddChild(horizontalLine);

            Button playButton = new Button("Nouvelle partie");
            playButton.OnClick = (GeonBit.UI.Entities.Entity button) =>
            {
                MainGame.instance.ChangeScene(new GameScene());
            };
            mainPanel.AddChild(playButton);

            Button quitButton = new Button("Quitter le jeu");
            quitButton.OnClick = (GeonBit.UI.Entities.Entity entity) =>
            {
                MainGame.instance.Exit();
            };
            mainPanel.AddChild(quitButton);

            UserInterface.Active.AddEntity(new Label("Cree par Romain", Anchor.BottomCenter));
        }

        public override void Update(float pDeltaTime)
        {
            
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            MainGame.instance.GraphicsDevice.Clear(Color.Black);
        }

        public override void Unload()
        {
            UserInterface.Active.Clear();
        }
    }
}
