﻿using Base_Game_Engine.Entities;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Scenes
{
    public abstract class Scene
    {
        protected Engine engine;

        public abstract void Load();
        public abstract void Unload();
        public abstract void Update(float pDeltaTime);
        public abstract void Draw(SpriteBatch pSpriteBatch);

        public void InitEngine()
        {
            engine = new Engine();
        }

        public void UpdateEngine(float pDeltaTime)
        {
            engine.Update(pDeltaTime);
        }

        public void DrawEngine(SpriteBatch pSpriteBatch)
        {
            engine.Draw(pSpriteBatch);
        }

        public Engine GetEngine()
        {
            return engine;
        }
    }
}
