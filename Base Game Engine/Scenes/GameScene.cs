﻿using Base_Game_Engine.Components;
using Base_Game_Engine.Entities;
using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity = Base_Game_Engine.Entities.Entity;

namespace Base_Game_Engine.Scenes
{
    public class GameScene : Scene
    {
        protected Panel pausePanel;

        public override void Load()
        {
            // Interface
            UserInterface.Active.ShowCursor = false;

            pausePanel = new Panel(new Vector2(400, 300), PanelSkin.Default, Anchor.Center);
            pausePanel.Visible = false;
            UserInterface.Active.AddEntity(pausePanel);

            Header header = new Header("PAUSE");
            pausePanel.AddChild(header);

            HorizontalLine horizontalLine = new HorizontalLine();
            pausePanel.AddChild(horizontalLine);

            Button backButton = new Button("Retour au jeu");
            backButton.OnClick = (GeonBit.UI.Entities.Entity button) =>
            {
                pausePanel.Visible = false;
                engine.SetUpdateActive(true);
                UserInterface.Active.ShowCursor = false;
            };
            pausePanel.AddChild(backButton);

            Button menuButton = new Button("Retour au menu");
            menuButton.OnClick = (GeonBit.UI.Entities.Entity entity) =>
            {
                MainGame.instance.ChangeScene(new MenuScene());
            };
            pausePanel.AddChild(menuButton);

            Button quitButton = new Button("Quitter le jeu");
            quitButton.OnClick = (GeonBit.UI.Entities.Entity entity) =>
            {
                MainGame.instance.Exit();
            };
            pausePanel.AddChild(quitButton);

            // Création du joueur
            Entity player = new Entity();

            // Positionnement du joueur
            player.x = MainGame.instance.GraphicsDevice.Viewport.Width / 2;
            player.y = MainGame.instance.GraphicsDevice.Viewport.Height / 2;

            // Ajout d'un composant graphique pour pouvoir l'afficher
            player.AddComponent(new SpriteRendererComponent(MainGame.instance.Content.Load<Texture2D>("Tom")));

            // Ajout d'une boîte de collision
            player.AddComponent(new BoxColliderComponent(31, 30, 0, 14));

            // Ajout du composant RigidBody pour ne pas qu'il traverse les murs
            player.AddComponent(new RigidBodyComponent());
            player.GetComponent<RigidBodyComponent>().AddGroup("player"); // On le met dans le groupe "player" pour que tout les RigidBody avec ce layer collisionne avec lui
            player.GetComponent<RigidBodyComponent>().AddLayer("wall"); // On veut collisionner avec les RigidBody du groupe "wall"

            // Ajout du composant "Joueur" pour pouvoir le déplacer au clavier
            player.AddComponent(new PlayerComponent());

            // Création d'un Layer "Entités"
            engine.AddLayer("entities", 1);

            // Ajout du joueur au layer "Entités"
            engine.AddEntity(player, "entities");

            // Création d'un mur
            Entity wall = new Entity();

            // Posionnement du mur
            wall.x = MainGame.instance.GraphicsDevice.Viewport.Width - MainGame.instance.GraphicsDevice.Viewport.Width / 4;
            wall.y = MainGame.instance.GraphicsDevice.Viewport.Height / 2;

            // Ajout d'un composant graphique pour pouvoir l'afficher
            wall.AddComponent(new BasicGraphicComponent(32, 128, Color.White));

            // Ajout d'une boîte de collision
            wall.AddComponent(new BoxColliderComponent(32, 128, 0, 0));

            // Ajout du composant RigidBody pour qu'il bloque le joueur
            wall.AddComponent(new RigidBodyComponent());
            wall.GetComponent<RigidBodyComponent>().AddGroup("wall"); // On le met dans le groupe "wall" pour que tout les RigidBody avec ce layer collisionne avec lui

            // Ajout du mur au layer "Par défaut"
            engine.AddEntity(wall);
        }

        public override void Update(float pDeltaTime)
        {
            // Entités mises à jour directement dans l'Engine

            // Menu Pause
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                pausePanel.Visible = true;
                engine.SetUpdateActive(false);
                UserInterface.Active.ShowCursor = true;
            }
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            // Entités affichées directement dans l'Engine
        }

        public override void Unload()
        {
            UserInterface.Active.Clear();
        }
    }
}
