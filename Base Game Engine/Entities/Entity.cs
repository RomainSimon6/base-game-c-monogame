﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Entities
{
    public class Entity
    {
        protected List<Component> listComponents;

        protected bool destroy;

        public float x;
        public float y;

        public Entity()
        {
            listComponents = new List<Component>();
            destroy = false;
        }

        public void AddComponent(Component pComponent)
        {
            pComponent.SetOwner(this);
            listComponents.Add(pComponent);
        }

        internal void AddComponent(object basicGraphi)
        {
            throw new NotImplementedException();
        }

        public void RemoveComponent(Component pComponent)
        {
            listComponents.Remove(pComponent);
        }

        public T GetComponent<T>() where T : Component
        {
            return (T) listComponents.Find(c => c.GetType() == typeof(T));
        }

        public void Update(float pDeltaTime)
        {
            foreach (Component component in listComponents)
            {
                if (component.IsEnabled())
                {
                    component.Update(pDeltaTime);
                }
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            foreach (Component component in listComponents)
            {
                if (component.IsEnabled())
                {
                    component.Draw(pSpriteBatch);
                }
            }
        }

        public void Destroy()
        {
            destroy = true;
        }

        public bool IsDestroyed()
        {
            return destroy;
        }
    }
}
