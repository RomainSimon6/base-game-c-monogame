﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Entities
{
    public class Engine
    {
        public List<Layer> listLayers;

        public bool updateActive;
        public bool drawActive;

        public Engine()
        {
            listLayers = new List<Layer>();
            updateActive = true;
            drawActive = true;
            AddLayer("default", 0);
        }

        public void SetUpdateActive(bool pActive)
        {
            updateActive = pActive;
        }

        public void SetDrawActive(bool pActive)
        {
            drawActive = pActive;
        }

        public void AddLayer(String pName, int pOrder)
        {
            if (!LayerExists(pName))
            {
                listLayers.Add(new Layer(pName, pOrder));
            }
        }

        public void RemoveLayer(String pName)
        {
            Layer layer = listLayers.Find(l => l.GetName() == pName);

            if (layer != null)
            {
                listLayers.Remove(layer);
            }
        }

        public bool LayerExists(String pName)
        {
            return listLayers.Find(l => l.GetName() == pName) != null;
        }

        public Layer GetLayer(String pName)
        {
            return listLayers.Find(l => l.GetName() == pName);
        }

        public void AddEntity(Entity pEntity, String pLayerName = "default")
        {
            GetLayer(pLayerName).AddEntity(pEntity);
        }

        public void RemoveEntity(Entity pEntity, String pLayerName = "default")
        {
            GetLayer(pLayerName).RemoveEntity(pEntity);
        }

        public List<Entity> GetEntities()
        {
            List<Entity> entities = new List<Entity>();

            foreach (Layer layer in listLayers)
            {
                entities.AddRange(layer.GetEntities());
            }

            return entities;
        }

        public void Update(float pDeltaTime)
        {
            if (!updateActive)
            {
                return;
            }

            foreach (Layer layer in listLayers.OrderBy(l => l.GetOrder()))
            {
                layer.Update(pDeltaTime);
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            if (!drawActive)
            {
                return;
            }

            foreach (Layer layer in listLayers.OrderBy(l => l.GetOrder()))
            {
                layer.Draw(pSpriteBatch);
            }
        }
    }
}
