﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Entities
{
    public class Layer
    {
        protected String name;
        protected int order;

        protected List<Entity> listEntities;

        public Layer(String pName, int pOrder)
        {
            name = pName;
            order = pOrder;
            listEntities = new List<Entity>();
        }

        public void AddEntity(Entity pEntity)
        {
            listEntities.Add(pEntity);
        }

        public void RemoveEntity(Entity pEntity)
        {
            listEntities.Remove(pEntity);
        }

        public List<Entity> GetEntities()
        {
            return listEntities;
        }

        public void Update(float pDeltaTime)
        {
            for (int i = listEntities.Count; i > 0; i--)
            {
                Entity entity = listEntities[i - 1];
                entity.Update(pDeltaTime);

                if (entity.IsDestroyed())
                {
                    listEntities.Remove(entity);
                }
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            foreach (Entity entity in listEntities)
            {
                entity.Draw(pSpriteBatch);
            }
        }

        public String GetName()
        {
            return name;
        }

        public int GetOrder()
        {
            return order;
        }
    }
}
