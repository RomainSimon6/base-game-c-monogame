﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Entities
{
    public abstract class Component
    {
        protected Entity owner;

        protected bool enabled;

        public abstract void Update(float pDeltaTime);
        public abstract void Draw(SpriteBatch pSpriteBatch);

        public void SetOwner(Entity pEntity)
        {
            owner = pEntity;
            enabled = true;
        }

        public Entity GetOwner()
        {
            return owner;
        }

        public void SetEnabled(bool pEnabled)
        {
            enabled = pEnabled;
        }

        public bool IsEnabled()
        {
            return enabled;
        }
    }
}
