﻿using Base_Game_Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Components
{
    public class BoxColliderComponent : Component
    {
        public int width;
        public int height;

        public int offsetX;
        public int offsetY;

        protected Rectangle boundingBox;

        public BoxColliderComponent(int pWidth, int pHeight, int pOffsetX, int pOffsetY)
        {
            width = pWidth;
            height = pHeight;
            offsetX = pOffsetX;
            offsetY = pOffsetY;

            boundingBox = new Rectangle(0, 0, 0, 0);
        }

        public override void Update(float pDeltaTime)
        {
            boundingBox = new Rectangle((int)owner.x - width / 2 + offsetX, (int)owner.y - height / 2 + offsetY, width, height);
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            Texture2D texture = new Texture2D(MainGame.instance.GraphicsDevice, 1, 1);
            texture.SetData(new Color[] { Color.Blue });

            pSpriteBatch.Draw(texture, boundingBox, new Color(1, 1, 1, 0.25f));
        }

        public Rectangle GetBoundingBox()
        {
            return boundingBox;
        }
    }
}
