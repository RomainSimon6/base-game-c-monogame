﻿using Base_Game_Engine.Entities;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Components
{
    public class PlayerComponent : Component
    {
        public PlayerComponent()
        {

        }

        public override void Update(float pDeltaTime)
        {
            float vx = 0;
            float vy = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                vy -= 60 * 2;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                vy += 60 * 2;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                vx -= 60 * 2;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                vx += 60 * 2;
            }

            owner.GetComponent<RigidBodyComponent>().MovePosition(vx, vy);
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            
        }
    }
}
