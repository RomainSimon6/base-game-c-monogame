﻿using Base_Game_Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Components
{
    public class SpriteRendererComponent : Component
    {
        protected Texture2D texture;

        public SpriteRendererComponent(Texture2D pTexture)
        {
            texture = pTexture;
        }

        public override void Update(float pDeltaTime)
        {
            
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(texture, new Vector2(owner.x - texture.Width / 2, owner.y - texture.Height / 2), Color.White);
        }
    }
}
