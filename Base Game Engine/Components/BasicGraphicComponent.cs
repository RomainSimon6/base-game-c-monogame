﻿using Base_Game_Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Components
{
    public class BasicGraphicComponent : Component
    {
        protected Texture2D texture;

        protected int width;
        protected int height;

        public BasicGraphicComponent(int pWidth, int pHeight, Color pColor)
        {
            width = pWidth;
            height = pHeight;

            texture = new Texture2D(MainGame.instance.GraphicsDevice, 1, 1);
            texture.SetData(new Color[] { pColor });
        }

        public override void Update(float pDeltaTime)
        {
            
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(texture, new Rectangle((int)owner.x - width / 2, (int)owner.y - height / 2, width, height), Color.White);
        }
    }
}
