﻿using Base_Game_Engine.Entities;
using Base_Game_Engine.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base_Game_Engine.Components
{
    public class RigidBodyComponent : Component
    {
        public float velocityX;
        public float velocityY;

        public List<String> group;
        public List<String> layer;

        public RigidBodyComponent()
        {
            group = new List<string>();
            layer = new List<string>();
        }

        public override void Update(float pDeltaTime)
        {
            BoxColliderComponent ownerBoxColliderComponent = owner.GetComponent<BoxColliderComponent>();
            
            if (ownerBoxColliderComponent == null)
            {
                owner.x += velocityX * pDeltaTime;
                owner.y += velocityY * pDeltaTime;

                return;
            }

            Rectangle ownerBoundingBox = ownerBoxColliderComponent.GetBoundingBox();
            Rectangle ownerHBoundingBox = new Rectangle((int)(ownerBoundingBox.X + velocityX * pDeltaTime), ownerBoundingBox.Y, ownerBoundingBox.Width, ownerBoundingBox.Height);
            Rectangle ownerVBoundingBox = new Rectangle(ownerBoundingBox.X, (int)(ownerBoundingBox.Y + velocityY * pDeltaTime), ownerBoundingBox.Width, ownerBoundingBox.Height);

            Scene currentScene = MainGame.instance.GetCurrentScene();
            List<Entity> entities = currentScene.GetEngine().GetEntities().FindAll(e => e != owner);
            List<Entity> entitiesWithRigidBodyComponent = entities.FindAll(e => e.GetComponent<RigidBodyComponent>() != null);
            List<Entity> entitiesInLayer = entitiesWithRigidBodyComponent.FindAll(e => e.GetComponent<RigidBodyComponent>().group.Find(l => layer.Contains(l)) != null);

            foreach (Entity entity in entitiesInLayer)
            {
                BoxColliderComponent entityBoxColliderComponent = entity.GetComponent<BoxColliderComponent>();

                if (entityBoxColliderComponent == null)
                {
                    continue;
                }

                if (ownerVBoundingBox.Intersects(entityBoxColliderComponent.GetBoundingBox()))
                {
                    if (velocityY < 0)
                    {
                        owner.y = entityBoxColliderComponent.GetBoundingBox().Y + entityBoxColliderComponent.GetBoundingBox().Height + ownerBoundingBox.Height / 2 - ownerBoxColliderComponent.offsetY;
                    }
                    else if (velocityY > 0)
                    {
                        owner.y = entityBoxColliderComponent.GetBoundingBox().Y - ownerBoundingBox.Height / 2 - ownerBoxColliderComponent.offsetY;
                    }

                    velocityY = 0;
                }

                if (ownerHBoundingBox.Intersects(entityBoxColliderComponent.GetBoundingBox()))
                {
                    if (velocityX < 0)
                    {
                        owner.x = entityBoxColliderComponent.GetBoundingBox().X + entityBoxColliderComponent.GetBoundingBox().Width + ownerBoundingBox.Width / 2 - ownerBoxColliderComponent.offsetX;
                    }
                    else if (velocityX > 0)
                    {
                        owner.x = entityBoxColliderComponent.GetBoundingBox().X - ownerBoundingBox.Width / 2 - ownerBoxColliderComponent.offsetX - 1;
                    }

                    velocityX = 0;
                }
            }

            owner.x += velocityX * pDeltaTime;
            owner.y += velocityY * pDeltaTime;
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            
        }

        public void AddGroup(String pGroup)
        {
            group.Add(pGroup);
        }

        public void AddLayer(String pLayer)
        {
            layer.Add(pLayer);
        }

        public void MovePosition(float pVelocityX, float pVelocityY)
        {
            velocityX = pVelocityX;
            velocityY = pVelocityY;
        }
    }
}
