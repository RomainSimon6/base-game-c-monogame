# Base Game Template

## Entités

Les *Entités* correspondent aux objets du jeu. Cela peut être le joueur, un ennemi, un tir, un mur, ...

Une *Entité* peut avoir un ou plusieurs *Composants*.

## Composants

Un *Composant* est relié à une *Entité*. Il s'agit d'un script qui permet d'étendre les propriétés et les actions de celle-ci.

Par exemple, l'*Entité* "Joueur" pourra voir un *Composant* "BoxColliderComponent" pour gérer les collisions, ou encore "PlayerComponent" pour déplacer le joueur avec les touches du clavier.

## Layers

Les *Layers* peuvent avoir zéro ou plusieurs *Entités*. Il servent à catégoriser celles-ci.

Par exemple, il est possible de créer un *Layer* "Joueur" qui aurait l'*Entité* "Joueur" et un *Layer* "Ennemis" qui aurait les *Entités* "Ennemis".

Ainsi, il est possible de récupérer les ennemis en récupérant les *Entités* présent dans le *Layer* "Ennemis".

## Engine

l'*Engine* est le cœur du système des *Entités*. Il permet de créer un nouveau *Layer* et se charge d'appeler les méthodes "Update" et "Draw" de tout les *Layers*.

## Composant RigidBodyComponent

Le *RigidBodyComponent*, associé à un *BoxColliderComponent* permet à une *Entité* de ne pas passer au travers d'une autre *Entité* avec ces mêmes *Composants*.

Un *RigidBodyComponent* possède un "Groupe" et un "Layer" (ce n'est pas le même *Layer* que vu précédemment.

Pour qu'une collision ai lieu, il faut que le groupe de l'*Entité* "A" corresponde au *Layer* de l'*Entité* "B".

Exemple :

- Player : Groupe : Player, Layer : Wall, Ennemies
- Wall : Groupe : Wall, Layer : None
- Ennemy : Groupe : Ennemies, Layer : Wall

Ainsi :

- Le joueur ne peut pas passer au travers d'un mur (le mur est du groupe "Wall" et le joueur à le Layer "Wall") ni d'un Ennemi.
- Les ennemis ne peuvent pas passer au travers d'un mur (idem que pour le joueur) mais peuvent passer au travers du joueur (le joueur est du groupe "Player" mais les ennemis n'ont pas la Layer "Player").
- Le mur n'a pas de Layer car il ne peut pas se déplacer, c'est donc inutile.